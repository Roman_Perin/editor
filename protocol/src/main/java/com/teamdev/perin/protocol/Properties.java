package com.teamdev.perin.protocol;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Properties implements Serializable{

    private Map<Header, String> properties = new HashMap<>();

    public String getProperty(Header key){
        return properties.get(key);
    }

    public void setProperty(Header key, String value){
        properties.put(key, value);
    }

    @Override
    public String toString(){
        return properties.toString();
    }

    public Properties copy(){

        Properties copy = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos);){

            oos.writeObject(this);
            oos.flush();
            baos.flush();
            try (ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
                 ObjectInputStream ois = new ObjectInputStream(bais);) {

                copy = (Properties) ois.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

        return copy;
    }
}
