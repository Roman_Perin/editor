package com.teamdev.perin.protocol;

import java.io.Serializable;

public enum Method implements Serializable{
    ADD,
    PASTE,
    REMOVE,
    COPY,
    CUT,
    OK,
    CLEAR,
    SET,
    UNDO,
    REDO,
    ERROR,
}
