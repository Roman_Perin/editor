package com.teamdev.perin.protocol;

import java.io.Serializable;

public interface Protocol<Data, Properties, Action extends Enum> extends Serializable{

    void setData(Data data);
    Data getData();

    void setPropertyOfData(Properties propertyOfData);
    Properties getPropertiesOfData();

    void setStartIndex(int startIndex);
    int getStartIndex();

    void setActionOnData(Action action);
    Action getActionOnData();

    void setLengthOfData(int length);
    int getLengthOfData();
}
