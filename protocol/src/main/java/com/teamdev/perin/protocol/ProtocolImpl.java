package com.teamdev.perin.protocol;


import java.io.Serializable;

public class ProtocolImpl implements Protocol<String, Properties, Method>, Serializable {

    private String textElement;
    private Properties propertyOfData;
    private int startIndex;
    private int lengthOfData;
    private Method action;


    @Override
    public void setData(String textElement) {
        this.textElement = textElement;
    }

    @Override
    public String getData() {
        return textElement;
    }

    @Override
    public void setPropertyOfData(Properties propertiesOfData) {
        this.propertyOfData = propertiesOfData;
    }

    @Override
    public Properties getPropertiesOfData() {
        return propertyOfData;
    }

    @Override
    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    @Override
    public int getStartIndex() {
        return startIndex;
    }

    @Override
    public void setActionOnData(Method action) {
        this.action = action;
    }

    @Override
    public Method getActionOnData() {
        return action;
    }

    @Override
    public void setLengthOfData(int length) {
        this.lengthOfData = length;
    }

    @Override
    public int getLengthOfData() {
        return lengthOfData;
    }

    @Override
    public String toString(){
        StringBuilder protocolRepresentation = new StringBuilder();
        protocolRepresentation.append("Protocol[textElement:").append(textElement).append(", ")
                              .append(propertyOfData).append(", startIndex:").append(startIndex)
                              .append(", lengthOfData:").append(lengthOfData).append(", Method:")
                              .append(action.toString()).append("]");
        return protocolRepresentation.toString();
    }

}
