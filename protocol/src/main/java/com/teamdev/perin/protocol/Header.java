package com.teamdev.perin.protocol;

import java.io.Serializable;

public enum Header implements Serializable{
    FONT,
    FONT_SIZE,
    ITALIC,
    BOLD,
    UNDERLINE,
    ALIGNMENT,
}
