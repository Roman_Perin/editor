package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.Properties;
import com.teamdev.perin.textservice.TextEditor;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class AddCommandTest {


    @Test (expected = IndexOutOfBoundsException.class)
    public void testAddingToPositionLessThanZero() {
        int destinationPosition = -1;
        Command add = new AddCommand(destinationPosition, "", new Properties());
        add.execute(new TextEditor());
        fail("IndexOutOfBoundsException was expected, but adding was done without exception.");
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testAddingToPositionGreaterThanSizeOfEditor() {
        int destinationPosition = 1;
        Command add = new AddCommand(destinationPosition, "", new Properties());
        add.execute(new TextEditor());
        fail("IndexOutOfBoundsException was expected, but adding was done without exception.");
    }

    @Test (expected = NullPointerException.class)
    public void testAddCommandParametersOnNull() {
        int destinationPosition = 1;
        Command add = new AddCommand(destinationPosition, null, null);
        add.execute(new TextEditor());
        fail("NullPointerException was expected, but adding with null arguments was done without exception.");
    }

    @Test
    public void testTwoAdditionsToTheSamePosition() {
        int destinationPosition = 0;
        TextEditor editor = new TextEditor();
        Command add1 = new AddCommand(destinationPosition, "a", new Properties());
        add1.execute(editor);
        Command add2 = new AddCommand(destinationPosition, "b", new Properties());
        add2.execute(editor);
        int expectedNumber = 2;
        String expectedZeroElement = "b";
        String expectedFirstElement = "a";
        assertEquals("Addition of two elements to the same position has to lead to availability of them from editor.",
                     expectedNumber, editor.getContent().size());
        assertEquals("Second added element has to be at the zero position.",
                     expectedZeroElement, editor.getContent().get(0).getData());
        assertEquals("First added element has to be at the first position.",
                     expectedFirstElement, editor.getContent().get(1).getData());
    }

}
