package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.TextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClearCommand implements Command {

    private static Logger logger = LoggerFactory.getLogger(ClearCommand.class.getName());

    @Override
    public void execute(TextEditor textEditor) {
        logger.trace("ClearCommand is executing.");
        textEditor.clear();
    }

    @Override
    public Command newInstance(ProtocolImpl request) {
        return new ClearCommand();
    }

    @Override
    public boolean isUndoable() {
        return true;
    }

    @Override
    public Command copy() {
        return new ClearCommand();
    }
}
