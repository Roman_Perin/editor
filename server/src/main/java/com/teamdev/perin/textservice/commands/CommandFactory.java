package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.Method;
import com.teamdev.perin.protocol.ProtocolImpl;

import java.util.HashMap;
import java.util.Map;

public class CommandFactory {

    private Map<Method, Command> controllers = new HashMap<Method, Command>(){
        {
            put(Method.ADD, new AddCommand());
            put(Method.PASTE, new PasteCommand());
            put(Method.REMOVE, new RemoveCommand());
            put(Method.COPY, new CopyCommand());
            put(Method.CUT, new CutCommand());
            put(Method.CLEAR, new ClearCommand());
            put(Method.SET, new SetCommand());
            put(Method.UNDO, new UndoCommand());
            put(Method.REDO, new RedoCommand());
        }
    };

    public Command createCommand(Method actionOnData, ProtocolImpl request) {
        return controllers.get(actionOnData).newInstance(request);
    }

}
