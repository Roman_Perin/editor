package com.teamdev.perin.textservice;

import com.teamdev.perin.editor.ContentElement;
import com.teamdev.perin.editor.ContentModel;
import com.teamdev.perin.protocol.Header;
import com.teamdev.perin.protocol.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ContentModelImpl implements ContentModel<String, Properties, Header, String> {

    private static Logger logger = LoggerFactory.getLogger(ContentModelImpl.class.getName());

    private List<ContentElement<String, Properties, Header, String>> elements = new ArrayList<>();


    @Override
    public void setProperties(int position, int length, Properties propertiesOfData) {
        Header[] propertyKeys = Header.values();
        for (int index = position; (index < (position + length)) && (index < elements.size()); index++) {
            setPropertyOfElement(propertiesOfData, propertyKeys, index);
        }
        logger.trace("New properties{} were set for data at positions:{}-to-{}.", propertiesOfData,
                     position, (position + length - 1));
    }

    @Override
    public void add(ContentElement<String, Properties, Header, String> elementToAdd, int destinationPosition) {
        elements.add(destinationPosition, elementToAdd);
        logger.trace("Element:{} was added to the position:{} of the ContentModel.", elementToAdd,
                     destinationPosition);
    }

    @Override
    public void add(ContentModel<String, Properties, Header, String> sequenceOfElements, int destinationPosition) {
        for(ContentElement<String, Properties, Header, String> element: sequenceOfElements){
            elements.add(destinationPosition, element);
            destinationPosition++;
        }
        logger.trace("Sequence of elements:{} was added to the position:{} of the ContentModel.", sequenceOfElements,
                     destinationPosition);
    }

    @Override
    public void add(ContentElement<String, Properties, Header, String> elementToAdd) {
        elements.add(elementToAdd);
        logger.trace("Element:{} was added to the end of the ContentModel.", elementToAdd);
    }

    @Override
    public ContentElement<String, Properties, Header, String> get(int fromPosition) {
        ContentElement<String, Properties, Header, String> gettingElement = elements.get(fromPosition);
        logger.trace("Element:{} is returning from the position:{}.", gettingElement, fromPosition);
        return gettingElement;
    }

    @Override
    public ContentModel<String, Properties, Header, String> get(int offset, int length) {
        ContentModel<String, Properties, Header, String> copyContent = new ContentModelImpl();
        while(length > 0 && offset < elements.size()){
            copyContent.add(elements.get(offset).copy());
            offset++;
            length--;
        }
        logger.trace("Elements:{} are returning.", copyContent);
        return copyContent;
    }

    @Override
    public ContentElement<String, Properties, Header, String> remove(int fromPosition) {
        logger.trace("Element is removing from the position:{}.", fromPosition);
        return elements.remove(fromPosition);
    }

    @Override
    public void remove(int from, int length) {
        while(length != 0){
            elements.remove(from);
            length--;
        }
        logger.trace("Elements were removed from positions:{}-{}.", from, (from+length-1));
    }

    @Override
    public void clear() {
        elements.clear();
        logger.trace("ContentModel was cleared.");
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public Iterator<ContentElement<String, Properties, Header, String>> iterator() {
        return elements.iterator();
    }

    @Override
    public String toString(){
        return elements.toString();
    }

    private void setPropertyOfElement(Properties propertiesOfData, Header[] propertyKeys, int index) {
        for (Header key : propertyKeys) {
            String property = propertiesOfData.getProperty(key);
            if (property != null){
                elements.get(index).setOneProperty(key, property);
            }
        }
    }
}
