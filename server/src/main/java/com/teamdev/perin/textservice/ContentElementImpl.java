package com.teamdev.perin.textservice;

import com.teamdev.perin.editor.ContentElement;
import com.teamdev.perin.protocol.Header;
import com.teamdev.perin.protocol.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ContentElementImpl implements ContentElement<String, Properties, Header, String> {

    private static Logger logger = LoggerFactory.getLogger(ContentElementImpl.class.getName());

    private String data;
    private Properties propertiesOfData;

    public ContentElementImpl(String data, Properties propertiesOfData) {
        this.data = data;
        this.propertiesOfData = propertiesOfData;
        logger.trace("Content element was instantiated with data:\'{}\' and properties:{}.", data, propertiesOfData);
    }

    @Override
    public String getData() {
        return data;
    }

    @Override
    public Properties getProperties() {
        logger.trace("ContentElement is returning it\'s properties:\'{}\'.", propertiesOfData);
        return propertiesOfData.copy();
    }

    @Override
    public void setProperties(Properties propertiesOfData) {
        this.propertiesOfData = propertiesOfData;
        logger.trace("Properties:{} were set.", propertiesOfData);
    }

    @Override
    public void setOneProperty(Header key, String value){
        propertiesOfData.setProperty(key, value);
        logger.trace("Property was set by the key:{} with value:{}.", key, value);
    }


    @Override
    public ContentElement<String, Properties, Header, String> copyPartOfElement(int from, int to) {
        String partOfData = data.substring(from, to);
        logger.trace("ContentElement is returning from data:\'{}\' copy of itself from position:{} to:{}. Part of it " +
                     "is:\'{}\'.", data, from, to, partOfData);
        return new ContentElementImpl(partOfData, propertiesOfData.copy());
    }

    @Override
    public ContentElement<String, Properties, Header, String> copy() {
        return new ContentElementImpl(data ,propertiesOfData.copy());
    }

    @Override
    public String toString(){
        return "Element[data: " + data + ", " + propertiesOfData + "]";
    }

}
