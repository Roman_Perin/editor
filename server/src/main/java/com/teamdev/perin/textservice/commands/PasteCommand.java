package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.TextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PasteCommand implements Command {

    private static Logger logger = LoggerFactory.getLogger(PasteCommand.class.getName());

    private int destinationPosition;

    public PasteCommand() {}

    public PasteCommand(int destinationPosition) {
        this.destinationPosition = destinationPosition;
    }


    @Override
    public void execute(TextEditor textEditor) {
        logger.trace("PasteCommand is executing with parameter:{}.", destinationPosition);
        textEditor.paste(destinationPosition);
    }

    @Override
    public Command newInstance(ProtocolImpl request) {
        return new PasteCommand(request.getStartIndex());
    }

    @Override
    public boolean isUndoable() {
        return true;
    }

    @Override
    public Command copy() {
        return new PasteCommand(destinationPosition);
    }

}
