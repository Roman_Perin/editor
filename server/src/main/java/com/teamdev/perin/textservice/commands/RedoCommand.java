package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.TextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedoCommand implements Command {

    private static Logger logger = LoggerFactory.getLogger(RedoCommand.class.getName());

    @Override
    public void execute(TextEditor textEditor) {
        textEditor.redo();
        logger.trace("RedoCommand is executing.");
    }

    @Override
    public Command newInstance(ProtocolImpl request) {
        return new RedoCommand();
    }

    @Override
    public boolean isUndoable() {
        return false;
    }

    @Override
    public Command copy() {
        return new RedoCommand();
    }
}
