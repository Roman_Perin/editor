package com.teamdev.perin.textservice;

import com.teamdev.perin.protocol.Method;

import java.util.HashMap;
import java.util.Map;

public class GeneratorOfResponsesFactory {


    private Map<Method, GeneratorOfResponses> GeneratorsOfResponses = new HashMap<Method, GeneratorOfResponses>(){
        {
            AcceptanceAcknowledgement acceptanceAcknowledgementGenerator = new AcceptanceAcknowledgement();
            CompleteResponse completeResponseGenerator = new CompleteResponse();

            put(Method.ADD, acceptanceAcknowledgementGenerator);
            put(Method.PASTE, completeResponseGenerator);
            put(Method.REMOVE, acceptanceAcknowledgementGenerator);
            put(Method.COPY, acceptanceAcknowledgementGenerator);
            put(Method.CUT, completeResponseGenerator);
            put(Method.CLEAR, acceptanceAcknowledgementGenerator);
            put(Method.SET, completeResponseGenerator);
            put(Method.UNDO, completeResponseGenerator);
            put(Method.REDO, completeResponseGenerator);
        }
    };

    public GeneratorOfResponses getGeneratorOfResponses(Method actionOnData) {
        return GeneratorsOfResponses.get(actionOnData);
    }

}
