package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.TextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CopyCommand implements Command {

    private static Logger logger = LoggerFactory.getLogger(CopyCommand.class.getName());

    private int offset;
    private int length;

    public CopyCommand() {
    }

    public CopyCommand(int offset, int length) {
        this.offset = offset;
        this.length = length;
    }


    @Override
    public void execute(TextEditor textEditor) {
        logger.trace("CopyCommand is executing.");
        textEditor.copy(offset, length);
    }

    @Override
    public Command newInstance(ProtocolImpl request) {
        return new CopyCommand(request.getStartIndex(), request.getLengthOfData());
    }

    @Override
    public boolean isUndoable() {
        return true;
    }

    @Override
    public Command copy() {
        return new CopyCommand(offset, length);
    }
}
