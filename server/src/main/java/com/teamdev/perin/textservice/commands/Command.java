package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.TextEditor;

public interface Command {

    void execute(TextEditor textEditor);

    Command newInstance(ProtocolImpl request);

    boolean isUndoable();

    Command copy();

}
