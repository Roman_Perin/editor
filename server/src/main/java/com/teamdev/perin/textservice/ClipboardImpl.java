package com.teamdev.perin.textservice;

import com.teamdev.perin.editor.Clipboard;
import com.teamdev.perin.editor.ContentElement;
import com.teamdev.perin.editor.ContentModel;
import com.teamdev.perin.protocol.Header;
import com.teamdev.perin.protocol.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClipboardImpl implements Clipboard<String, Properties, Header, String> {

    private static Logger logger = LoggerFactory.getLogger(ClipboardImpl.class.getName());

    private ContentModel<String, Properties, Header, String> temporaryCopyOfElements = new ContentModelImpl();

    @Override
    public void add(ContentElement<String, Properties, Header, String> temporaryCopy) {
        temporaryCopyOfElements.add(temporaryCopy);
        logger.trace("Element:{} was added to the Clipboard.", temporaryCopy);
    }

    @Override
    public void add(ContentModel<String, Properties, Header, String> copyContent) {
        temporaryCopyOfElements = copyContent;
        logger.trace("List of Elements:{} was added to the Clipboard.", copyContent);
    }

    @Override
    public ContentModel getAll() {
        logger.trace("All elements:{} are returning from the Clipboard.", temporaryCopyOfElements);
        return temporaryCopyOfElements;
    }

    @Override
    public void clear() {
        temporaryCopyOfElements.clear();
        logger.trace("Clipboard was cleared.");
    }

    @Override
    public String toString(){
        return temporaryCopyOfElements.toString();
    }
}
