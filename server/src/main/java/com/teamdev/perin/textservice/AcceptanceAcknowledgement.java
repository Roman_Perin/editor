package com.teamdev.perin.textservice;

import com.teamdev.perin.protocol.Method;
import com.teamdev.perin.protocol.ProtocolImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class AcceptanceAcknowledgement extends GeneratorOfResponses {

    private static Logger logger = LoggerFactory.getLogger(AcceptanceAcknowledgement.class.getName());

    @Override
    public List<ProtocolImpl> generate(TextEditor textEditor) {

        List<ProtocolImpl> responses = new ArrayList<>();
        ProtocolImpl response = new ProtocolImpl();

        response.setActionOnData(Method.OK);
        responses.add(response);

        logger.trace("Acceptance acknowledgement responses were generated:{}.", responses);
        return responses;
    }

}
