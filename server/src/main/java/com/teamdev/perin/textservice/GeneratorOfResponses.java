package com.teamdev.perin.textservice;

import com.teamdev.perin.protocol.ProtocolImpl;

import java.util.List;

public abstract class GeneratorOfResponses {

    public abstract List<ProtocolImpl> generate(TextEditor textEditor);

}
