package com.teamdev.perin.textservice;

import com.teamdev.perin.editor.ContentElement;
import com.teamdev.perin.editor.ContentModel;
import com.teamdev.perin.protocol.Header;
import com.teamdev.perin.protocol.Method;
import com.teamdev.perin.protocol.Properties;
import com.teamdev.perin.protocol.ProtocolImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CompleteResponse extends  GeneratorOfResponses {

    private static Logger logger = LoggerFactory.getLogger(CompleteResponse.class.getName());

    @Override
    public List<ProtocolImpl> generate(TextEditor textEditor) {

        List<ProtocolImpl> responses = new ArrayList<ProtocolImpl>();
        ProtocolImpl responseForClearing = new ProtocolImpl();

        responseForClearing.setActionOnData(Method.CLEAR);
        responses.add(responseForClearing);

        ContentModel<String, Properties, Header, String> content = textEditor.getContent();
        for (ContentElement<String, Properties, Header, String> textElement : content) {
            ProtocolImpl response = createResponse(textElement);
            responses.add(response);
        }
        logger.trace("CompleteResponse have generated list of responses:{}", responses);
        return responses;
    }


    private ProtocolImpl createResponse(ContentElement<String, Properties, Header, String> textElement) {

        ProtocolImpl response = new ProtocolImpl();

        response.setData(textElement.getData());
        response.setActionOnData(Method.ADD);
        response.setPropertyOfData(textElement.getProperties());

        return response;
    }

}

