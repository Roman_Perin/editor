package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.Properties;
import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.ContentElementImpl;
import com.teamdev.perin.textservice.TextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddCommand implements Command {

    private static Logger logger = LoggerFactory.getLogger(AddCommand.class.getName());

    private int destinationPosition;
    private String dataForAdding;
    private Properties propertiesOfData;


    public AddCommand() {
    }

    public AddCommand(int destinationPosition, String dataForAdding, Properties propertiesOfData) {
        this.destinationPosition = destinationPosition;
        this.dataForAdding = dataForAdding;
        this.propertiesOfData = propertiesOfData;
    }


    @Override
    public void execute(TextEditor textEditor) {
        ContentElementImpl dataElement = new ContentElementImpl(dataForAdding, propertiesOfData.copy());
        logger.trace("AddCommand is executing with next parameters: data:{}," +
                     " positionToAdd:{}, properties:{}", dataForAdding, destinationPosition, propertiesOfData);
        textEditor.add(dataElement, destinationPosition);
    }

    @Override
    public Command newInstance(ProtocolImpl request) {
        return new AddCommand(request.getStartIndex(), request.getData(), request.getPropertiesOfData());
    }

    @Override
    public boolean isUndoable() {
        return true;
    }

    @Override
    public Command copy() {
        return new AddCommand(destinationPosition, dataForAdding, propertiesOfData.copy());
    }
}
