package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.TextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoveCommand implements Command {

    private static Logger logger = LoggerFactory.getLogger(RemoveCommand.class.getName());

    private int from;
    private int length;

    public RemoveCommand() {}

    public RemoveCommand(int from, int length) {
        this.from = from;
        this.length = length;
    }


    @Override
    public void execute(TextEditor textEditor) {
        logger.trace("RemoveCommand is executing with next parameters: from:{}, length:{}.", from, length);
        textEditor.remove(from, length);
    }

    @Override
    public Command newInstance(ProtocolImpl request) {
        return new RemoveCommand(request.getStartIndex(), request.getLengthOfData());
    }

    @Override
    public boolean isUndoable() {
        return true;
    }

    @Override
    public Command copy() {
        return new RemoveCommand(from, length);
    }

}
