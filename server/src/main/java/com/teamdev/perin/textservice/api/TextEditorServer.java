package com.teamdev.perin.textservice.api;

import com.teamdev.perin.protocol.Method;
import com.teamdev.perin.protocol.Properties;
import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.GeneratorOfResponses;
import com.teamdev.perin.textservice.GeneratorOfResponsesFactory;
import com.teamdev.perin.textservice.TextEditor;
import com.teamdev.perin.textservice.commands.Command;
import com.teamdev.perin.textservice.commands.CommandFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

public class TextEditorServer implements Server<String, Properties, Method, Integer> {

    private static Logger logger = LoggerFactory.getLogger(TextEditorServer.class.getName());

    private TextEditor textEditor = new TextEditor();
    private CommandFactory commands = new CommandFactory();
    private GeneratorOfResponsesFactory generators = new GeneratorOfResponsesFactory();


    @Override
    public void acceptRequest(ByteArrayOutputStream clientsSender, ByteArrayOutputStream serversSender){

        try(ByteArrayInputStream receiver = new ByteArrayInputStream(clientsSender.toByteArray());
            ObjectInputStream requestReceiver = new ObjectInputStream(receiver);) {

            ProtocolImpl request = (ProtocolImpl)requestReceiver.readObject();
            logger.trace("Request:{} was received.", request);

            List<ProtocolImpl> responses = acceptRequest(request);
            sendResponses(responses, serversSender);
            logger.trace("All responses were sent.");
        } catch (IOException e) {
            ProtocolImpl errorResponse = new ProtocolImpl();
            errorResponse.setActionOnData(Method.ERROR);
            errorResponse.setData("Problems with request receiving were occurred.");
            logger.debug("There was an exception during request receiving.", e);
            sendSingleResponse(serversSender, errorResponse);
        } catch (ClassNotFoundException e) {
            ProtocolImpl errorResponse = new ProtocolImpl();
            errorResponse.setActionOnData(Method.ERROR);
            errorResponse.setData("Current browser version is unsupported.");
            logger.debug("Unknown protocol was received.", e);
            sendSingleResponse(serversSender, errorResponse);
        }
    }


    private List<ProtocolImpl> acceptRequest(ProtocolImpl request) {
        Command requestedCommand = commands.createCommand(request.getActionOnData(), request);
        requestedCommand.execute(textEditor);
        textEditor.getUndoManager().addCommand(requestedCommand.copy());
        GeneratorOfResponses generator = generators.getGeneratorOfResponses(request.getActionOnData());
        return generator.generate(textEditor);
    }

    private void sendResponses(List<ProtocolImpl> responses, ByteArrayOutputStream senderOfResponses) {
        for (ProtocolImpl response : responses) {
            sendSingleResponse(senderOfResponses, response);
            logger.trace("Response:{} was sent.", response);
        }
    }

    private void sendSingleResponse(ByteArrayOutputStream senderOfResponses, ProtocolImpl response) {
        try( ObjectOutputStream oos = new ObjectOutputStream(senderOfResponses);) {
            oos.writeObject(response);
            oos.flush();
            senderOfResponses.flush();
        } catch (IOException e) {
            logger.debug("Impossible to send back response.", e);
        }
    }

}
