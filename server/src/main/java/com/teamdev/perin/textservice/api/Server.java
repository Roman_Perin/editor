package com.teamdev.perin.textservice.api;

import java.io.ByteArrayOutputStream;

public interface Server<Data, Properties, Action extends Enum, Identifier> {

    public void acceptRequest(ByteArrayOutputStream requests, ByteArrayOutputStream senderOfResponses);

}
