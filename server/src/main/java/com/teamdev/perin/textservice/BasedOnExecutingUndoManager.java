package com.teamdev.perin.textservice;

import com.teamdev.perin.editor.UndoManager;
import com.teamdev.perin.textservice.commands.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


public class BasedOnExecutingUndoManager implements UndoManager<Command> {

    private List<Command> undoCommands = new ArrayList<>();
    private Deque<Command> redoCommands = new ArrayDeque<>();
    private TextEditor textEditor;

    public BasedOnExecutingUndoManager(TextEditor textEditor) {
        this.textEditor = textEditor;
    }

    private static Logger logger = LoggerFactory.getLogger(BasedOnExecutingUndoManager.class.getName());

    @Override
    public void undo() {
        if (!undoCommands.isEmpty()) {
            int lastIndex = undoCommands.size()-1;
            logger.trace("Command:{} is unexecuting.", undoCommands.get(lastIndex));
            redoCommands.push(undoCommands.remove(lastIndex));
            textEditor.clear();
            executeAllUndoCommands();
        }
    }

    @Override
    public void redo() {
        if (!redoCommands.isEmpty()) {
            Command commandForReexecution = redoCommands.pop();
            commandForReexecution.execute(textEditor);
            logger.trace("Command:{} was reexecuted.", commandForReexecution);
            undoCommands.add(commandForReexecution);
        }
    }

    @Override
    public void addCommand(Command command) {
        if (command.isUndoable()) {
            redoCommands.clear();
            undoCommands.add(command);
            logger.trace("Command:{} was accepted by the undo manager.", command);
        }
    }

    private void executeAllUndoCommands() {
        logger.trace("Execution of undo commands is going on.");
        for (Command undoCommand : undoCommands) {
            undoCommand.execute(textEditor);
        }
    }

}
