package com.teamdev.perin.textservice;

import com.teamdev.perin.editor.Editor;
import com.teamdev.perin.protocol.Header;
import com.teamdev.perin.protocol.Properties;
import com.teamdev.perin.textservice.commands.Command;

public class TextEditor extends Editor<String, Properties, Command, Header, String> {

    @Override
    public ContentModelImpl createContentModel() {
        return new ContentModelImpl();
    }

    @Override
    public ClipboardImpl createClipboard() {
        return new ClipboardImpl();
    }

    @Override
    public BasedOnExecutingUndoManager createUndoManager() {
        return new BasedOnExecutingUndoManager(this);
    }
}
