package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.TextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UndoCommand implements Command {

    private static Logger logger = LoggerFactory.getLogger(UndoCommand.class.getName());

    @Override
    public void execute(TextEditor textEditor) {
        logger.trace("UndoCommand is executing.");
        textEditor.undo();
    }

    @Override
    public Command newInstance(ProtocolImpl request) {
        return new UndoCommand();
    }

    @Override
    public boolean isUndoable() {
        return false;
    }

    @Override
    public Command copy() {
        return new UndoCommand();
    }

}
