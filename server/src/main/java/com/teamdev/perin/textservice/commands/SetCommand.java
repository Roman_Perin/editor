package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.Properties;
import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.TextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetCommand implements Command{

    private static Logger logger = LoggerFactory.getLogger(SetCommand.class.getName());

    private int destinationPosition;
    private int length;
    private Properties newProperties;

    public SetCommand() {}

    public SetCommand(int destinationPosition, int length, Properties newProperties) {
        this.destinationPosition = destinationPosition;
        this.length = length;
        this.newProperties = newProperties;
    }


    @Override
    public void execute(TextEditor textEditor) {
        logger.trace("SetCommand is executing with next parameters: fromPosition:{}, numberOfPositions:{}, " +
                     "properties:{}.", destinationPosition, length, newProperties);
        textEditor.setProperties(destinationPosition, length, newProperties.copy());
    }

    @Override
    public Command newInstance(ProtocolImpl request) {
        return new SetCommand(request.getStartIndex(), request.getLengthOfData(), request.getPropertiesOfData());
    }

    @Override
    public boolean isUndoable() {
        return true;
    }

    @Override
    public Command copy() {
        return new SetCommand(destinationPosition, length, newProperties.copy());
    }

}
