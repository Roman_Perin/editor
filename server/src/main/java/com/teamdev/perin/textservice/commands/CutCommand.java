package com.teamdev.perin.textservice.commands;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textservice.TextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CutCommand implements Command {

    private static Logger logger = LoggerFactory.getLogger(CutCommand.class.getName());

    private int offset;
    private int length;

    public CutCommand() {
    }

    public CutCommand(int offset, int length) {
        this.offset = offset;
        this.length = length;
    }


    @Override
    public void execute(TextEditor textEditor) {
        logger.trace("CutCommand is executing with next parameters: offset:{}, length:{}.", offset, length);
        textEditor.cut(offset, length);
    }

    @Override
    public Command newInstance(ProtocolImpl request) {
        return new CutCommand(request.getStartIndex(), request.getLengthOfData());
    }

    @Override
    public boolean isUndoable() {
        return true;
    }

    @Override
    public Command copy() {
        return new CutCommand(offset, length);
    }

}
