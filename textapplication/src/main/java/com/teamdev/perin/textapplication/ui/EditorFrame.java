package com.teamdev.perin.textapplication.ui;

import com.teamdev.perin.protocol.Header;
import com.teamdev.perin.protocol.Method;
import com.teamdev.perin.protocol.Properties;
import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textapplication.Client;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class EditorFrame extends JFrame {

    private ButtonFactory buttonsOfMenu = new ButtonFactory(".//textapplication//src//main//resources//");
    private LogicTextPane text = new LogicTextPane();
    JLabel statusLine = new JLabel("STATUS");
    private Client client = new Client(statusLine, text);
//    TODO - shortKey


    public EditorFrame() throws HeadlessException {
        setTitle("Victoria");
        setNativeLookAndFeel();
        Toolkit kit = Toolkit.getDefaultToolkit();
        //Default size is setting.
//        todo: Set Full screen again.
//        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setSize(980, 200);
        //Icon is setting.
        Image icon = kit.getImage(".//textapplication//src//main//resources//icon.png");
        setIconImage(icon);
        setResizable(true);

        //Menu is setting.
        JMenuBar menu = new JMenuBar();
        setJMenuBar(menu);
        JMenu file = new JMenu("File");
        JMenu edit = new JMenu("Edit");
        menu.add(file);
        menu.add(edit);
        JMenuItem undo = new JMenuItem("Undo");
        final JMenuItem redo = new JMenuItem("Redo");
        edit.add(undo);
        edit.add(redo);

        //Layout is setting.
        GridBagLayout layout = new GridBagLayout();
        setLayout(layout);

        //Buttons of toolbar.
        JButton buttonCutSelection = buttonsOfMenu.createButton("cut_32.png", "Cut the selection and put it on the Clipboard.");
        JButton buttonCopySelection = buttonsOfMenu.createButton("edit-copy.png", "Copy the selection and put it on the Clipboard.");
        JButton buttonPasteSelection = buttonsOfMenu.createButton("edit-paste.png", "Paste the contents of the Clipboard.");
        final JButton buttonSetBold = buttonsOfMenu.createButton("bold.png", "Change to a bold font.");
        final JButton buttonSetItalic = buttonsOfMenu.createButton("italic.png", "Change to an italic font.");
        final JButton buttonSetUnderline = buttonsOfMenu.createButton("underline.png", "Change to an underline font.");
        final JButton buttonSetLeftAlignment = buttonsOfMenu.createButton("justify-left.png", "Align text to the left.");
        final JButton buttonSetRightAlignment = buttonsOfMenu.createButton("justify-right.png", "Align text to the right.");
        final JButton buttonSetCentreAlignment = buttonsOfMenu.createButton("justify-center.png", "Align text to the centre.");
        final String toolTipOfJustification = "Align text to both left and right margins, adding extra space between words " +
                "as necessary. This creates a clean look along the left and right side of the page.";
        final JButton buttonSetJustifiedAlignment = buttonsOfMenu.createButton("justify-fill.png", toolTipOfJustification);

        //Combo boxes of toolbar.
        ComboBoxFactory comboBoxesOfMenu = new ComboBoxFactory();
        String[] availableFonts = new String[]{"Arial", "Calibri", "Candara","Corbel",
                                               "Courier New", "Tahoma", "Times New Roman", "Verdana"};
        String defaultFont = "Arial";
        final JComboBox<String> fonts = comboBoxesOfMenu.createComboBox(availableFonts, defaultFont,
                                                                        "Change the font family.");
        Integer[] availableFontSizes = new Integer[]{8, 10, 12, 14, 16, 18, 20, 24, 32, 48, 60, 72};
        Integer defaultFontSize = 14;
        final JComboBox<Integer> sizesOfFonts = comboBoxesOfMenu.createComboBox(availableFontSizes, defaultFontSize,
                                                                                "Change the font size.");

        //Adding components to toolbar.
        final JToolBar toolBarOfEditing = new JToolBar();
        toolBarOfEditing.setFloatable(false);
        toolBarOfEditing.add(buttonCutSelection);
        toolBarOfEditing.add(buttonCopySelection);
        toolBarOfEditing.add(buttonPasteSelection);

        JToolBar propertiesOfFonts = new JToolBar();
        propertiesOfFonts.setFloatable(false);
        propertiesOfFonts.add(buttonSetBold);
        propertiesOfFonts.add(buttonSetItalic);
        propertiesOfFonts.add(buttonSetUnderline);

        final JToolBar aligns = new JToolBar();
        aligns.setFloatable(false);
        createToolBar(aligns, buttonSetLeftAlignment, buttonSetCentreAlignment,
                      buttonSetRightAlignment, buttonSetJustifiedAlignment);

        //TextArea
        text.requestFocusInWindow();
        final Style basicStyle = text.addStyle("Basic", null);
        StyleConstants.setFontFamily(basicStyle, (String) fonts.getSelectedItem());
        StyleConstants.setFontSize(basicStyle, (Integer) sizesOfFonts.getSelectedItem());
        text.setCharacterAttributes(basicStyle, true);
        JScrollPane scroll = new JScrollPane(text);

        //Status Line
        JLabel statusLineLabel = new JLabel("Status line:");
        JPanel statusLine = new JPanel();
        FlowLayout statusLayout = new FlowLayout();
        statusLayout.setAlignment(FlowLayout.LEFT);
        statusLine.setLayout(statusLayout);
        statusLayout.setHgap(10);

        statusLine.add(statusLineLabel);
        statusLine.add(this.statusLine);

        //The placement of visual components.
        add(toolBarOfEditing, new GridBagConstraintsBuilder().cellCoordinates(1, 0).insetFromLeft(5).insetFromTop(5)
                .anchor(GridBagConstraints.LINE_START).build());
        add(createSeparator(), new GridBagConstraintsBuilder().cellCoordinates(2, 0).insetFromLeft(5).insetFromTop(5)
                .anchor(GridBagConstraints.WEST).build());
        add(fonts, new GridBagConstraintsBuilder().cellCoordinates(3, 0).insetFromLeft(10).insetFromTop(5)
                .anchor(GridBagConstraints.LINE_START).fill(GridBagConstraints.HORIZONTAL).weightX(1).build());
        add(sizesOfFonts, new GridBagConstraintsBuilder().cellCoordinates(4, 0).insetFromLeft(2).insetFromTop(5)
                .build());
        add(createSeparator(), new GridBagConstraintsBuilder().cellCoordinates(5, 0).insetFromLeft(10).insetFromTop(5)
                .anchor(GridBagConstraints.WEST).build());
        add(propertiesOfFonts, new GridBagConstraintsBuilder().cellCoordinates(6, 0).insetFromTop(5)
                .anchor(GridBagConstraints.WEST).insetFromLeft(2).build());
        add(createSeparator(), new GridBagConstraintsBuilder().cellCoordinates(7, 0).insetFromTop(5)
                .anchor(GridBagConstraints.WEST).insetFromLeft(5).build());
        add(aligns, new GridBagConstraintsBuilder().cellCoordinates(8, 0).insetFromTop(5)
                .anchor(GridBagConstraints.WEST).insetFromLeft(5).insetFromRight(10).build());

        add(scroll, new GridBagConstraintsBuilder().cellCoordinates(0, 1).weightY(1).numberOfColumnsPerCell
                (GridBagConstraints.REMAINDER).insetFromRight(10).insetFromLeft(10).fill(GridBagConstraints.BOTH)
                .insetFromTop(5).build());

        add(statusLine, new GridBagConstraintsBuilder().cellCoordinates(0, 2).insetFromLeft(0).insetFromRight(10)
                .insetFromTop(1).weightY(0).numberOfColumnsPerCell(GridBagConstraints.REMAINDER)
                .fill(GridBagConstraints.BOTH).build());


        //Adding Listeners to visual components.
        undo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendRequest(Method.UNDO);
            }
        });

        redo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendRequest(Method.REDO);
            }
        });


        buttonCutSelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int start = text.getSelectionStart();
                int end = text.getSelectionEnd();
                if (start != end) {
                    ProtocolImpl request = generateProtocol(Method.CUT, start, end - start);
                    sendRequest(request);
                }
                text.setCaretPosition(start);
            }
        });

        buttonCopySelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int start = text.getSelectionStart();
                int end = text.getSelectionEnd();
                if (start != end) {
                    ProtocolImpl request = generateProtocol(Method.COPY, start, end - start);
                    sendRequest(request);
                }
                text.setCaretPosition(end);
            }
        });

        buttonPasteSelection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int caretPosition = text.getCaretPosition();
                ProtocolImpl request = generateRequest(Method.PASTE, text.getCaretPosition());
                sendRequest(request);
                text.setCaretPosition(caretPosition);
            }
        });


        fonts.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent event) {

                String newFont = (String)fonts.getSelectedItem();
                int start = text.getSelectionStart();
                int end = text.getSelectionEnd();
                if (start != end) {
                    ProtocolImpl request = generateProtocol(Method.SET, start, (end - start));
                    request.getPropertiesOfData().setProperty(Header.FONT, newFont);
                    sendRequest(request);
                }
                text.setCaretPosition(end);
                text.requestFocusInWindow();
            }
        });

        sizesOfFonts.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {

                int newSize = (Integer) (sizesOfFonts.getSelectedItem());
                int start = text.getSelectionStart();
                int end = text.getSelectionEnd();
                if (start != end) {
                    ProtocolImpl request = generateProtocol(Method.SET, start, (end - start));
                    request.getPropertiesOfData().setProperty(Header.FONT_SIZE, Integer.toString(newSize));
                    sendRequest(request);
                }
                text.setCaretPosition(end);
                text.requestFocusInWindow();
            }
        });


        buttonSetBold.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int start = text.getSelectionStart();
                int end = text.getSelectionEnd();
                if (start != end) {
                    boolean isBold = StyleConstants.isBold(text.getInputAttributes());
                    ProtocolImpl request = generateProtocol(Method.SET, start, (end - start));
                    request.getPropertiesOfData().setProperty(Header.BOLD, Boolean.toString(!isBold));
                    sendRequest(request);
                }
                text.setCaretPosition(end);
                text.requestFocusInWindow();
            }
        });

        buttonSetItalic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int start = text.getSelectionStart();
                int end = text.getSelectionEnd();
                if (start != end) {
                    boolean isItalic = StyleConstants.isItalic(text.getInputAttributes());
                    ProtocolImpl request = generateProtocol(Method.SET, start, (end - start));
                    request.getPropertiesOfData().setProperty(Header.ITALIC, Boolean.toString(!isItalic));
                    sendRequest(request);
                }
                text.setCaretPosition(end);
                text.requestFocusInWindow();
            }
        });

        buttonSetUnderline.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int start = text.getSelectionStart();
                int end = text.getSelectionEnd();
                if (start != end) {
                    boolean isUnderline = StyleConstants.isUnderline(text.getInputAttributes());
                    ProtocolImpl request = generateProtocol(Method.SET, start, (end - start));
                    request.getPropertiesOfData().setProperty(Header.UNDERLINE, Boolean.toString(!isUnderline));
                    sendRequest(request);
                }
                text.setCaretPosition(end);
                text.requestFocusInWindow();
            }
        });

        //Listeners for Alignments.
        buttonSetLeftAlignment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int startIndex = text.getSelectionStart();
                int endIndex = text.getSelectionEnd();
                StyledDocument document = text.getStyledDocument();
                int startOfLine = getStartOfLine(startIndex, document);
                int endOfLine = getEndOfLine(endIndex, document);
                ProtocolImpl request = generateProtocol(Method.SET, startOfLine, (endOfLine - startOfLine));
                request.getPropertiesOfData().setProperty(Header.ALIGNMENT,
                                                          Integer.toString(StyleConstants.ALIGN_LEFT));
                sendRequest(request);
                text.setCaretPosition(endIndex);
                text.requestFocusInWindow();
            }
        });

        buttonSetCentreAlignment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int startIndex = text.getSelectionStart();
                int endIndex = text.getSelectionEnd();
                StyledDocument document = text.getStyledDocument();
                int startOfLine = getStartOfLine(startIndex, document);
                int endOfLine = getEndOfLine(endIndex, document);
                ProtocolImpl request = generateProtocol(Method.SET, startOfLine, (endOfLine - startOfLine));
                request.getPropertiesOfData().setProperty(Header.ALIGNMENT,
                                                          Integer.toString(StyleConstants.ALIGN_CENTER));
                sendRequest(request);
                text.setCaretPosition(endIndex);
                text.requestFocusInWindow();
            }
        });

        buttonSetRightAlignment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int startIndex = text.getSelectionStart();
                int endIndex = text.getSelectionEnd();
                StyledDocument document = text.getStyledDocument();
                int startOfLine = getStartOfLine(startIndex, document);
                int endOfLine = getEndOfLine(endIndex, document);
                ProtocolImpl request = generateProtocol(Method.SET, startOfLine, (endOfLine - startOfLine));
                request.getPropertiesOfData().setProperty(Header.ALIGNMENT,
                                                          Integer.toString(StyleConstants.ALIGN_RIGHT));
                sendRequest(request);
                text.setCaretPosition(endIndex);
                text.requestFocusInWindow();
            }
        });

        buttonSetJustifiedAlignment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int startIndex = text.getSelectionStart();
                int endIndex = text.getSelectionEnd();
                StyledDocument document = text.getStyledDocument();
                int startOfLine = getStartOfLine(startIndex, document);
                int endOfLine = getEndOfLine(endIndex, document);
                ProtocolImpl request = generateProtocol(Method.SET, startOfLine, (endOfLine - startOfLine));
                request.getPropertiesOfData().setProperty(Header.ALIGNMENT,
                        Integer.toString(StyleConstants.ALIGN_JUSTIFIED));
                sendRequest(request);
                text.setCaretPosition(endIndex);
                text.requestFocusInWindow();
            }
        });

        //Listeners of adding and removing characters.
        text.getStyledDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (!text.isServerAdding()){
                    int start = e.getOffset();
                    int length = e.getLength();
                    for (int offset = start; offset < start + length; offset++) {
                        StyledDocument document = text.getStyledDocument();
                        AttributeSet fromCurrentAttributes = document.getCharacterElement(text.getCaretPosition())
                                                                     .getAttributes();
                        ProtocolImpl request = generateProtocol(Method.ADD, offset, document, fromCurrentAttributes);
                        sendRequest(request);
                    }
                }
                text.setServerIsAdding(false);
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                if (!text.isServerRemoving()) {
                    int offset = e.getOffset();
                    int length = e.getLength();
                    Method methodOfRequest = Method.REMOVE;
                    StyledDocument document = text.getStyledDocument();
                    if (document.getLength() == 0) {
                        methodOfRequest = Method.CLEAR;
                    }
                    ProtocolImpl request = generateProtocol(methodOfRequest, offset, length);
                    sendRequest(request);
                }
                text.setServerIsRemoving(false);
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
    }


    private void sendRequest(Method method){
        ProtocolImpl request = new ProtocolImpl();
        request.setActionOnData(method);
        sendRequest(request);
    }

    private void sendRequest(ProtocolImpl request){
        try {
            client.sendRequest(request);
        } catch (IOException e1) {
            statusLine.setText("Request sending failed.");
        }
    }

    private int getStartOfLine(int startIndex, Document document) {
        int currentPosition = startIndex;
        int lengthOfChar = 1;
                while (currentPosition > 0){
                    currentPosition--;
                    String str = null;
                    try {
                        str = document.getText(currentPosition, lengthOfChar);
                    } catch (BadLocationException e1) {
                        e1.printStackTrace();
                    }
                    if ("\n".equals(str)){
                        return ++currentPosition;
                    }
                }
        return currentPosition;
    }

    private int getEndOfLine(int endIndex, Document document) {
        int currentPosition = endIndex;
        int lengthOfChar = 1;
        while (currentPosition < document.getLength()){
            String str = null;
            try {
                str = document.getText(currentPosition, lengthOfChar);
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
            if ("\n".equals(str)){
                return ++currentPosition;
            }
            currentPosition++;
        }
        return currentPosition;
    }

    private ProtocolImpl generateProtocol(Method action, int offset, int length) {
        ProtocolImpl request = new ProtocolImpl();
        request.setStartIndex(offset);
        request.setActionOnData(action);
        request.setLengthOfData(length);
        request.setPropertyOfData(new Properties());
        return request;
    }

    private ProtocolImpl generateRequest(Method method, int offset) {
        ProtocolImpl request = new ProtocolImpl();
        request.setActionOnData(method);
        request.setStartIndex(offset);
        return request;
    }

    private ProtocolImpl generateProtocol(Method method, int offset, StyledDocument document,
                                          AttributeSet fromCurrentAttributes) {
        int minimalLength = 1;
        ProtocolImpl request = new ProtocolImpl();
        Properties properties = generateProperties(document, fromCurrentAttributes);
        try {
            request.setData(document.getText(offset, minimalLength));
        } catch (BadLocationException e) {
            statusLine.setText("Some problems with your browser. Try update it or call to our hot line.");
        }
        request.setPropertyOfData(properties);
        request.setActionOnData(method);
        request.setStartIndex(offset);
        request.setLengthOfData(minimalLength);
        return request;
    }

    private Properties generateProperties(StyledDocument document, AttributeSet fromCurrentAttributes) {
        Properties properties = new Properties();

        String font = document.getFont(fromCurrentAttributes).getFamily();
        int fontSize = document.getFont(fromCurrentAttributes).getSize();
        boolean isBold = document.getFont(fromCurrentAttributes).isBold();
        boolean isItalic = document.getFont(fromCurrentAttributes).isItalic();
        boolean isUnderline = StyleConstants.isUnderline(fromCurrentAttributes);
        int alignment = StyleConstants.getAlignment(fromCurrentAttributes);

        properties.setProperty(Header.FONT, font);
        properties.setProperty(Header.FONT_SIZE, Integer.toString(fontSize));
        properties.setProperty(Header.BOLD, Boolean.toString(isBold));
        properties.setProperty(Header.ITALIC, Boolean.toString(isItalic));
        properties.setProperty(Header.UNDERLINE, Boolean.toString(isUnderline));
        properties.setProperty(Header.ALIGNMENT, Integer.toString(alignment));

        return properties;
    }

    private <T extends Component> T[] createToolBar(JToolBar aligns, T... toolBarComponents) {
        for (T toolBarComponent : toolBarComponents) {
            aligns.add(toolBarComponent);
        }
        return toolBarComponents;
    }

    private JToolBar createSeparator() {
        JToolBar separator = new JToolBar();
        separator.setFloatable(false);
        separator.setRollover(false);
        JButton delimiter1 = buttonsOfMenu.createButton("blank.png", "");
        JButton delimiter2 = buttonsOfMenu.createButton("blank.png", "");
        separator.add(delimiter1);
        separator.addSeparator();
        separator.add(delimiter2);
        return separator;
    }

    private void setNativeLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e) {
            //TODO insert logger for logging of error. System.out.println("Error setting native LAF: " + e);
        }
    }

}
