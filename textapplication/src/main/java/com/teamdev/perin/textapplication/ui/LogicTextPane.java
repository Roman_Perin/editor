package com.teamdev.perin.textapplication.ui;

import javax.swing.*;

public class LogicTextPane extends JTextPane {

    private boolean serverIsAdding = false;
    private boolean serverIsRemoving = false;

    public boolean isServerAdding(){
        return serverIsAdding;
    }

    public boolean isServerRemoving(){
        return serverIsRemoving;
    }

    public void setServerIsAdding(boolean serverIsAdding){
        this.serverIsAdding = serverIsAdding;
    }

    public void setServerIsRemoving(boolean serverIsRemoving){
        this.serverIsRemoving = serverIsRemoving;
    }

}
