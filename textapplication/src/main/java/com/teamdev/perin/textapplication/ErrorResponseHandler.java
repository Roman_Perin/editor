package com.teamdev.perin.textapplication;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textapplication.ui.LogicTextPane;

import javax.swing.*;

public class ErrorResponseHandler implements ResponseHandler {

    @Override
    public void handle(ProtocolImpl response, LogicTextPane pane, JLabel statusLine) {
        String errorMessage = response.getActionOnData().toString() + ": " + response.getData();
        statusLine.setText(errorMessage);
    }
}
