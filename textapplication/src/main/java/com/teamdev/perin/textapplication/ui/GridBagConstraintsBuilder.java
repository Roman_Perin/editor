package com.teamdev.perin.textapplication.ui;

import java.awt.*;

public class GridBagConstraintsBuilder {

    private int gridX;
    private int gridY;
    private int insetFromTop;
    private int insetFromLeft;
    private int insetFromBottom;
    private int insetFromRight;
    private int widthPadding;
    private int heightPadding;

    //default values
    private int numberOfColumnsPerCell = 1;
    private int numberOfRowsPerCell = 1;
    private int anchor = GridBagConstraints.CENTER;
    private int fill = GridBagConstraints.NONE;
    private int weightX = 0;
    private int weightY = 0;


    public GridBagConstraintsBuilder() {
    }

    public GridBagConstraintsBuilder cellCoordinates(int gridX, int gridY){
        this.gridX = gridX;
        this.gridY = gridY;
        return this;
    }

    public GridBagConstraintsBuilder numberOfColumnsPerCell(int numberOfColumnsPerCell){
        this.numberOfColumnsPerCell = numberOfColumnsPerCell;
        return this;
    }

    public GridBagConstraintsBuilder numberOfRowsPerCell(int numberOfRowsPerCell){
        this.numberOfRowsPerCell = numberOfRowsPerCell;
        return this;
    }

    public GridBagConstraintsBuilder anchor(int anchor){
        this.anchor = anchor;
        return this;
    }

    public GridBagConstraintsBuilder fill(int fill){
        this.fill = fill;
        return this;
    }

    public GridBagConstraintsBuilder weightX(int weightX){
        this.weightX = weightX;
        return this;
    }

    public GridBagConstraintsBuilder weightY(int weightY){
        this.weightY = weightY;
        return this;
    }

    public GridBagConstraintsBuilder insetFromTop(int insetFromTop){
        this.insetFromTop = insetFromTop;
        return this;
    }

    public GridBagConstraintsBuilder insetFromLeft(int insetFromLeft){
        this.insetFromLeft = insetFromLeft;
        return this;
    }

    public GridBagConstraintsBuilder insetFromBottom(int insetFromBottom){
        this.insetFromBottom = insetFromBottom;
        return this;
    }

    public GridBagConstraintsBuilder insetFromRight(int insetFromRight){
        this.insetFromRight = insetFromRight;
        return this;
    }

    public GridBagConstraintsBuilder insets(int bordersSpace){
        this.insetFromBottom = bordersSpace;
        this.insetFromLeft = bordersSpace;
        this.insetFromRight = bordersSpace;
        this.insetFromTop = bordersSpace;
        return this;
    }

    public GridBagConstraintsBuilder widthPadding(int widthPadding){
        this.widthPadding = widthPadding;
        return this;
    }

    public GridBagConstraintsBuilder heightPadding(int heightPadding){
        this.heightPadding = heightPadding;
        return this;
    }


    public GridBagConstraints build(){

        GridBagConstraints output = new GridBagConstraints();

        output.gridx = this.gridX;
        output.gridy = this.gridY;
        output.gridwidth = this.numberOfColumnsPerCell;
        output.gridheight = this.numberOfRowsPerCell;
        output.weightx = this.weightX;
        output.weighty = this.weightY;
        output.anchor = this.anchor;
        output.fill = this.fill;
        output.insets = new Insets(insetFromTop, insetFromLeft, insetFromBottom, insetFromRight);
        output.ipadx = this.widthPadding;
        output.ipady = this.heightPadding;

        return output;
    }

}
