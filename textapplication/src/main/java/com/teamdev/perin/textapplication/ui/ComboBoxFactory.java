package com.teamdev.perin.textapplication.ui;

import javax.swing.*;

public class ComboBoxFactory {

    private JComboBox comboBox;

    public <T> JComboBox<T> createComboBox(T[] list, Object selectedItem, String toolTip){
        comboBox = new JComboBox(list);
        comboBox.setToolTipText(toolTip);
        comboBox.setEditable(false);
        comboBox.setSelectedItem(selectedItem);
        comboBox.setFocusable(false);
        return comboBox;
    }

}
