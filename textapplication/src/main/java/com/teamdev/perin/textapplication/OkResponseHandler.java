package com.teamdev.perin.textapplication;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textapplication.ui.LogicTextPane;

import javax.swing.*;

public class OkResponseHandler implements ResponseHandler {

    @Override
    public void handle(ProtocolImpl response, LogicTextPane pane, JLabel statusLine) {
        statusLine.setText(response.getActionOnData().toString());
    }
}
