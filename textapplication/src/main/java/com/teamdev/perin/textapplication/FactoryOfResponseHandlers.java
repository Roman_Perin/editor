package com.teamdev.perin.textapplication;

import com.teamdev.perin.protocol.Method;

import java.util.HashMap;
import java.util.Map;

public class FactoryOfResponseHandlers {

    private Map<Method, ResponseHandler> responseHandlers = new HashMap<Method, ResponseHandler>() {
        {
            put(Method.OK, new OkResponseHandler());
            put(Method.CLEAR, new ClearResponseHandler());
            put(Method.ADD, new AddResponseHandler());
            put(Method.ERROR, new ErrorResponseHandler());
        }
    };


    public ResponseHandler getHandler(Method actionOnData) {
        return responseHandlers.get(actionOnData);
    }
}
