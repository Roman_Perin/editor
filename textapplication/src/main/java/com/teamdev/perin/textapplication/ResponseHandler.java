package com.teamdev.perin.textapplication;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textapplication.ui.LogicTextPane;

import javax.swing.*;

public interface ResponseHandler {

   void handle(ProtocolImpl response, LogicTextPane pane, JLabel label);

}
