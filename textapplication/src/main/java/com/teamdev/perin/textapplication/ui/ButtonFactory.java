package com.teamdev.perin.textapplication.ui;

import javax.swing.*;
import java.awt.*;

public class ButtonFactory extends JButton {

    private String path;// = ".//userInterface//src//main//resources//";

    public ButtonFactory(String path) {
        super();
        this.path = path;
    }

    public JButton createButton(String nameOfIcon, String toolTip) {
        JButton button = new JButton();
        button.setToolTipText(toolTip);
        button.setIcon(new ImageIcon(path + nameOfIcon));
        button.setMargin(new Insets(0, 0, 0, 0));
        button.setFocusable(false);
        return button;
    }

}
