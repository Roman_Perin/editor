package com.teamdev.perin.textapplication;

import com.teamdev.perin.protocol.ProtocolImpl;
import com.teamdev.perin.textapplication.ui.LogicTextPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

public class ClearResponseHandler implements ResponseHandler {

    private static Logger logger = LoggerFactory.getLogger(ClearResponseHandler.class.getName());

    @Override
    public void handle(ProtocolImpl response, LogicTextPane pane, JLabel statusLine) {

        StyledDocument document = pane.getStyledDocument();

        try {
            pane.setServerIsRemoving(true);
            pane.getStyledDocument().remove(0, document.getLength());
            statusLine.setText(response.getActionOnData().toString());
            logger.trace("Document was cleared.");
        } catch (BadLocationException e) {
            String message = "Impossible to clear document.";
            logger.debug(message, e);
            statusLine.setText(message);
        }
    }
}
