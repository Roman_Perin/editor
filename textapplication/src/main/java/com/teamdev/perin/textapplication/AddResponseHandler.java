package com.teamdev.perin.textapplication;

import com.teamdev.perin.protocol.*;
import com.teamdev.perin.textapplication.ui.LogicTextPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.text.*;

public class AddResponseHandler implements ResponseHandler {

    private static Logger logger = LoggerFactory.getLogger(AddResponseHandler.class.getName());


    @Override
    public void handle(ProtocolImpl response, LogicTextPane pane, JLabel statusLine) {

        StyledDocument document = pane.getStyledDocument();
        MutableAttributeSet attributesOfText = new SimpleAttributeSet();

        Properties properties = response.getPropertiesOfData();
        StyleConstants.setFontFamily(attributesOfText, properties.getProperty(Header.FONT));
        StyleConstants.setFontSize(attributesOfText, Integer.valueOf(properties.getProperty(Header.FONT_SIZE)));
        StyleConstants.setAlignment(attributesOfText, Integer.valueOf(properties.getProperty(Header.ALIGNMENT)));

        StyleConstants.setBold(attributesOfText, Boolean.valueOf(properties.getProperty(Header.BOLD)));
        StyleConstants.setItalic(attributesOfText, Boolean.valueOf(properties.getProperty(Header.ITALIC)));
        StyleConstants.setUnderline(attributesOfText, Boolean.valueOf(properties.getProperty(Header.UNDERLINE)));


        try {
            int offset = document.getLength();
            pane.setServerIsAdding(true);
            document.insertString(offset, response.getData(), attributesOfText);
            document.setParagraphAttributes(offset, document.getLength(), attributesOfText, false);
            statusLine.setText(response.getActionOnData().toString());
            logger.trace("Text \'{}\' was added with properties:{}.", response.getData(), properties);
        } catch (BadLocationException e) {
            logger.debug("Impossible to render information:{}, from " +
                         "position:{}.", response.getData(), document.getLength(), e);
            statusLine.setText("Impossible to render information.");
        }
    }

}
