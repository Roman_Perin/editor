package com.teamdev.perin.editor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Editor<Data, Properties, Command, Property, PropertyValue> {

    private Logger logger = LoggerFactory.getLogger(Editor.class);

    private ContentModel<Data, Properties, Property, PropertyValue> content = createContentModel();
    private Clipboard<Data, Properties, Property, PropertyValue> clipboard = createClipboard();
    private UndoManager<Command> undoManager = createUndoManager();

    //TODO: To think if an operations on the document correspond to the editor.
/*
    create();

    open();

    save();
*/
    public abstract ContentModel<Data,Properties, Property, PropertyValue> createContentModel();

    public abstract Clipboard<Data,Properties, Property, PropertyValue> createClipboard();

    public abstract UndoManager<Command> createUndoManager();


    public void add(ContentElement<Data, Properties, Property, PropertyValue> elementToAdd, int position){
        content.add(elementToAdd, position);
        logger.trace("Data:{} was added to the position:{}.", elementToAdd.getData(), position);
    }

    public void paste(int position){
        content.add(clipboard.getAll(), position);
        logger.trace("Content of clipboard was added to the position:{}.", position);
    }

    public void copy(int from, int length) {
        clipboard.clear();
        ContentModel<Data, Properties, Property, PropertyValue> copyContent = content.get(from, length);
        clipboard.add(copyContent);
        logger.trace("Elements were copied from positions:{}-to-{} to the clipboard.", from, (from + length - 1));
    }

    public void cut(int offset, int length) {
        copy(offset, length);
        content.remove(offset, length);
        logger.trace("Elements were cut from positions:{}-to-{}.", offset, (offset+length-1));
    }

    public void clear(){
        content.clear();
        logger.trace("Container of elements was cleared.");
    }

    public void remove(int offset, int length) {
        content.remove(offset, length);
        logger.trace("Elements were deleted from positions:{}-to-{}.", offset, (offset+length-1));
    }

    public void setProperties(int position, int length, Properties propertiesOfData) {
        content.setProperties(position, length, propertiesOfData);
    }

    public void undo(){
        undoManager.undo();
        logger.trace("Undo was executed.");
    }

    public void redo(){
        undoManager.redo();
        logger.trace("Redo was executed.");
    }

    public ContentModel<Data, Properties, Property, PropertyValue> getContent(){
        return content;
    }

    public UndoManager<Command> getUndoManager(){
        return undoManager;
    }

}
