package com.teamdev.perin.editor;

public interface Clipboard<Data, Properties, Property, PropertyValue> {

    void add(ContentElement<Data, Properties, Property, PropertyValue> temporaryCopy);

    void add(ContentModel<Data, Properties, Property, PropertyValue> copyContent);

    ContentModel<Data, Properties, Property, PropertyValue> getAll();

    void clear();
}
