package com.teamdev.perin.editor;

public interface ContentElement<Data, Properties, Property, PropertyValue> {

    Data getData();

    Properties getProperties();

    void setProperties(Properties propertiesOfData);

    void setOneProperty(Property key, PropertyValue value);

    ContentElement<Data, Properties, Property, PropertyValue> copyPartOfElement(int from, int to);

    ContentElement<Data, Properties, Property, PropertyValue> copy();


}
