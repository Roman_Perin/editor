package com.teamdev.perin.editor;

public interface UndoManager<Command> {

    void undo();

    void redo();

    void addCommand(Command command);

}
