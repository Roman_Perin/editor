package com.teamdev.perin.editor;

public interface ContentModel<Data, Properties, Property, PropertyValue> extends Iterable<ContentElement<Data,
                                                                                 Properties, Property, PropertyValue>> {

    void setProperties(int position, int length, Properties propertiesOfData);

    void add(ContentElement<Data, Properties, Property, PropertyValue> elementToAdd, int destinationPosition);

    void add(ContentModel<Data, Properties, Property, PropertyValue> sequenceOfElements, int destinationPosition);

    void add(ContentElement<Data, Properties, Property, PropertyValue> elementToAdd);

    ContentElement<Data, Properties, Property, PropertyValue> get(int fromPosition);

    ContentModel<Data, Properties, Property, PropertyValue> get(int from, int length);

    ContentElement<Data, Properties, Property, PropertyValue> remove(int fromPosition);

    void remove(int from, int length);

    void clear();

    int size();
}
